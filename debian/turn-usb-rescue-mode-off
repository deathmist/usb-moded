#!/bin/sh

# This script mimics Maemo's init-done process where a file is placed and a
# couple of signals are fired. This is needed as usb-moded are hard-coded
# to look for these before allowing any mode switch to be done (and to also
# disable rescue mode). Signalling `com.meego.usb_moded.rescue_off` is not
# enough. (Why do you give that signal then???)
#
# Code is taken from [1] with some modification. According to [2], this code is
# in Public Domain. I (Ratchanan while on UBports Foundation duty) hereby allow
# (minimal) modification to be in Public Domain as well (or if not applicable,
# CC0).
#
# [1] https://github.com/nemomobile/nemo-mobile-session/blob/master/bin/init-done
# [2] https://github.com/nemomobile/nemo-mobile-session/blob/master/rpm/nemo-mobile-session.spec

INIT_DONE_FILE=/run/systemd/boot-status/init-done

# Mark init-done timing to the file only once per boot
if [ ! -f $INIT_DONE_FILE ]; then
    SEC_NOW=$(cat /proc/uptime | cut -d " " -f1)
    echo "INIT DONE in $SEC_NOW s"
    mkdir -p "$(dirname $INIT_DONE_FILE)"
    echo "$SEC_NOW" > $INIT_DONE_FILE
fi

# Inform mce, dsme and others (e.g. usb-moded) that boot is over
dbus-send --system --type=signal /com/nokia/startup/signal com.nokia.startup.signal.runlevel_switch_done "int32:5"
dbus-send --system --type=signal /com/nokia/startup/signal com.nokia.startup.signal.init_done "int32:5"
