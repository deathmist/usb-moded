Source: usb-moded
Section: misc
Priority: optional
Maintainer: Philippe De Swert <philippe.de-swert@nokia.com>
Build-Depends: debhelper-compat (= 12),
               autoconf,
               automake,
               dbus <!nocheck>,
               dh-exec,
               libdbus-1-dev,
               libdbus-glib-1-dev,
               libglib2.0-dev,
               libkmod-dev,
               libsystemd-dev,
               doxygen,
               libudev-dev,
Standards-Version: 3.9.1
Homepage: https://github.com/sailfishos/usb-moded
Vcs-Browser: https://gitlab.com/ubports/core/packaging/usb-moded
Vcs-Git: https://gitlab.com/ubports/core/packaging/usb-moded.git

Package: usb-moded
Architecture: linux-any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         dbus,
         lsof,
         net-tools,
Description: usb_moded - USB mode controller
 Usb_moded is a daemon to control the USB states. For this
 it loads unloads the relevant usb gadget modules, keeps track
 of the filesystem(s) and notifies about changes on the DBUS
 system bus.

Package: usb-moded-doc
Section: doc
Architecture: all
Depends: usb-moded (= ${binary:Version}),
         ${misc:Depends},
Description: usb_moded - USB mode controller - documentation
 Usb_moded is a daemon to control the USB states. For this
 it loads unloads the relevant usb gadget modules, keeps track
 of the filesystem(s) and notifies about changes on the DBUS
 system bus.
 === This package contains the documentation ===

Package: usb-moded-dev
Architecture: linux-any
Depends: ${misc:Depends},
         libdbus-1-dev,
Description: usb_moded - USB mode controller - development files
 Usb_moded is a daemon to control the USB states. For this
 it loads unloads the relevant usb gadget modules, keeps track
 of the filesystem(s) and notifies about changes on the DBUS
 system bus.
 === This package contains the files needed to program for usb_moded ===

Package: usb-moded-ubports-config
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         adbd,
         busybox,
         gettext-base,
         libglib2.0-bin,
         lxc-android-config,
         network-manager,
         openssh-server,
         usb-moded (>= ${source:Version}), usb-moded (<< ${source:Version}.1~),
# Contains /etc/udhcpd.conf, which conflicts with necessary link that
# usb-moded is hard-coded to check.
Conflicts: udhcpd
Description: usb_moded - USB mode controller - UBports configuration
 Usb_moded is a daemon to control the USB states. For this
 it loads unloads the relevant usb gadget modules, keeps track
 of the filesystem(s) and notifies about changes on the DBUS
 system bus.
 .
 This package contains UBports-specific configuration for usb-moded,
 for use with Ubuntu Touch. This package also contains a configurator
 which auto-detect some settings and interpolate them into the
 configuration.
